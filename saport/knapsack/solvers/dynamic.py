from ..abstractsolver import AbstractSolver
from ..model import Solution
from numpy.typing import ArrayLike
import numpy as np
from typing import Tuple


class DynamicSolver(AbstractSolver):
    """
    A naive dynamic programming solver for the knapsack problem.
    """

    def create_table(self) -> ArrayLike:
        # TODO: fill the table!
        # tip 1. init table using np.zeros function
        # tip 2. remember to handle timeout (refer to the dfs solver for an example)
        #        - just return the current state of the table
        table = np.zeros([self.problem.capacity+1, len(self.problem.items)+1])
        obiekty = self.problem.items
        for kolumna in range(len(self.problem.items)+1):
            for wiersz in range(self.problem.capacity+1):
                if kolumna == 0:
                    continue
                elif obiekty[kolumna-1].weight > wiersz:
                    table[wiersz][kolumna] = table[wiersz][kolumna-1]
                else:
                    table[wiersz][kolumna] = max(table[wiersz-obiekty[kolumna-1].weight][kolumna-1]+obiekty[kolumna-1].value, table[wiersz][kolumna-1])
        # print("----------------")
        # print(table)
        # print("----------------")
        return table

    def extract_solution(self, table: ArrayLike) -> Solution:
        used_items = []
        optimal = table[-1, -1] > 0

        # print("----------------")
        # print(table)
        # print("----------------")
        # TODO: extract taken items from the table!
        wiersz = self.problem.capacity
        for i in range(len(self.problem.items), 0, -1):
            if table[wiersz][i] != table[wiersz][i-1]:
                used_items.append(self.problem.items[i-1])
                wiersz -= self.problem.items[i-1].weight

        return Solution.from_items(used_items, optimal)

    def solve(self) -> Tuple[Solution, float]:
        self.interrupted = False
        self.start_timer()

        table = self.create_table()
        solution = self.extract_solution(table) if table is not None else Solution.empty()

        self.stop_timer()
        return solution
