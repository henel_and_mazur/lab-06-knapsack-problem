from ..abstractsolver import AbstractSolver
from ..model import Solution, Item
from typing import List


class AbstractBnbSolver(AbstractSolver):
    """
    An abstract branch-and-bound solver for the knapsack problems.

    Methods:
    --------
    upper_bound(left : List[Item], solution: Solution) -> float:
        given the list of still available items and the current solution,
        calculates the linear relaxation of the problem
    """

    def upper_bound(self, left: List[Item], solution: Solution) -> float:
        # TODO: implement the linear relaxation, i.e. assume you can take
        #      fraction of the items in the backpack
        #      return the value of such a solution
        #      tip 1. solution is your "starting point" (items already in the backpack)
        #      tip 2. left is the list of items you can still take
        #      tip 3. take the items with highest value density first (as in greedy_density approach)

        sumOfValues = solution.value
        sumOfweights = solution.weight
        sortedList = dict()
        for j in range(len(left)):
            highestDensity = 0
            chosenItem = None
            for i in range(len(left)):
                if left[i].value/left[i].weight > highestDensity and i not in sortedList.keys():
                    highestDensity = left[i].value/left[i].weight
                    chosenItem = i
            if chosenItem is not None:
                for k in range(len(left)):
                    if left[k].value/left[k].weight == highestDensity:
                        sortedList.update({k: left[k]})
                        if left[k].weight + sumOfweights <= self.problem.capacity:
                            sumOfweights += left[k].weight
                            sumOfValues += left[k].value
                        else:
                            if sumOfweights == self.problem.capacity:
                                return sumOfValues
                            sumOfValues += (self.problem.capacity - sumOfweights)/left[k].weight * left[k].value
                            return sumOfValues

        return sumOfValues

    def solve(self) -> Solution:
        raise Exception("this is an abstract solver, don't try to run it!")
